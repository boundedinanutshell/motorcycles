angular.module('app', ['ngRoute', 'ngResource'])

.config(function($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'MainCtrl',
            templateUrl: '/static/main.html'
        })
        .otherwise({
            redirectTo: '/'
        });
})

.factory('Bikes', function($resource, $q) {
    return {
        load: function(params) {
            var deferred = $q.defer();
            $resource('/bikes').get({'params': params}, function(response) {
                deferred.resolve(response.data);
            });
            return deferred.promise;
        },

        loadPics: function(url) {
            var deferred = $q.defer();
            $resource('/pics').get({'url': url}, function(response) {
                deferred.resolve(response.data);
            });
            return deferred.promise;
        }


    };
})

.filter('capitalize', function() {
    return function(input) {
        if (!input) {
            return '';
        }
        return input.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    };
})

.filter('formatNumber', function() {
    return function (input, decimals) {
        var dec = !!decimals && angular.isNumber(decimals) ? decimals : 0;
        if (!!input || input === 0) {
            return (String((+input).toFixed(dec))).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        } else {
            return '';
        }
    };
})

.controller('MainCtrl', function($scope, Bikes) {
    $scope.fields = ['price', 'title', 'year', 'make', 'engine', 'date']
    $scope.sortField = 'price';
    $scope.sortReverse = false;
    $scope.loading = false;
    $scope.params = {}
    $scope.bools = {'makeUnknown': true};
    $scope.origin = {};
    $scope.makeFilter = {};
    $scope.page = 1;
    $scope.bikes = [];

    $scope.showSearchOptions = $scope.showFilterOptions = true;

    $scope.presetQueries = {
        'Motorcycles only': '-mini -scooter -scooters -gokart -goped -cart -vespa -moped -quad -atv -zuma -kart -trailer -trike -taotao'
    };

    $scope.makes = {
        'Japanese': ['yamaha', 'honda', 'suzuki', 'kawasaki', 'fuji'],
        'American': ['harley', 'buell', 'cushman', 'indian', 'victory', 'american ironhorse'],
        'UK': ['triumph', 'bsa'],
        'Taiwanese': ['sym', 'kymco'],
        'Italian': ['ducati', 'aprilia'],
        'German': ['bmw', 'maico'],
        'Other': ['bultaco', 'taotao', 'tomos', 'ktm', 'hyosung']
        /*
        'Spanish': ['bultaco'],
        'Slovenian': ['tomos'],
        'Austrian': ['ktm'],
        'Chinese': ['taotao'],
        'South Korea': ['hyosung']*/
    };

    angular.forEach($scope.makes, function(makes, origin) {
        $scope.origin[origin] = true;
        for (var i=0; i<makes.length; i++) {
            $scope.makeFilter[makes[i]] = true;
        }
    });

    $scope.query = function() {
        event.stopPropagation();
        $scope.bikes = [];
        $scope.sortField = null;
        $scope.sortReverse = false;
        $scope.page = 1;
        $scope.loading = true;
        Bikes.load($scope.params).then(function(data) {
            $scope.loading = false;
            $scope.bikes = data;
        });
    };

    $scope.loadMore = function() {
        event.stopPropagation();
        $scope.loading = true;
        var params = $scope.params;
        params['s'] = $scope.page * 100;
        $scope.page += 1;
        Bikes.load(params).then(function(data) {
            $scope.loading = false;
            $scope.bikes = $scope.bikes.concat(data);
        });
    };

    $scope.loadPics = function(bike) {
        Bikes.loadPics(bike.url).then(function(data) {
            $scope.bike['pics'] = data;
        });
    };

    $scope.setSort = function(field) {
        $scope.sortReverse = $scope.sortField === field ? !$scope.sortReverse : false;
        $scope.sortField = field;
    };

    $scope.filter = function(bike) {
        var fields = ['engine', 'year', 'make'];
        for (var i=0; i<fields.length; i++) {
            var field = fields[i];
            if (!( ($scope.bools[field+'Unknown'] && !bike[field]) || 
                 ( (field !== 'make' || $scope.makeFilter[bike[field]])
                && (!$scope.bools[field+'Min'] || bike[field] >= $scope.bools[field+'Min']) 
                && (!$scope.bools[field+'Max'] || bike[field] <= $scope.bools[field+'Max']) ) )) {
                return false;
            }
        }
        return true;
    };

    $scope.toggleOrigin = function(origin) {
        var bool = !$scope.origin[origin];
        for (var i=0; i < $scope.makes[origin].length; i++) {
            var make = $scope.makes[origin][i];
            $scope.makeFilter[make] = bool;
        }
    };

});


