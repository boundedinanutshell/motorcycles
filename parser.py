from BeautifulSoup import BeautifulSoup
import requests
import re

class Parser:

    def __init__(self):
        self = self

    def formatUrl(self, params):
        url = 'http://sfbay.craigslist.org/search/mca?'
        if params.items:
            for key, value in params.items():
                url += key + '=' + str(value) + '&'
        return url

    def printBikeInfo(self, params):
        print params['title']

        print (params['year'] + ' ' if 'year' in params else '') +\
            (params['make'] + ' ' if 'make' in params else '') +\
            (str(params['engine']) + 'cc ' if 'engine' in params else '') +\
            ('for $' + str(params['price']) if 'price' in params else '' )

        print ''

    def parse(self, params):
        url = self.formatUrl(params)

        r = requests.get(url)
        soup = BeautifulSoup(r.text)
        import pdb;pdb.set_trace()

        rows = soup.select('.row')

        makes = ['yamaha', 'honda', 'suzuki', 'harley', 'triumph', 'bmw', 'sym',
            'bultaco', 'kymco', 'kawasaki', 'ducati', 'tomos', 'aprilia', 'buell',
            'ktm', 'indian', 'victory', 'can am', 'cushman', 'vespa', 'bsa',
            'taotao', 'maico', 'american ironhorse', 'hyosung', 'fuji']

        bikes = []
        for row in rows:
            params = {}
            title = row.select('.pl')[0].select('a')[0].text
            params['title'] = title;
            params['url'] = 'http://www.craigslist.org/' + row.select('.i')[0]['href']
            params['date'] = row.select('.date')[0].text
            params['pic'] = row.select('.p')[0].text != ''
            title = title.lower()

            year = re.search('(19|20)\d{2}', title)
            if year:
                params['year'] = year.group()

            if len(row.select('.price')) > 0:
                price = row.select('.price')[0].string
                params['price'] = int(price[1:])

            engine = re.search('\d{3,4}?cc', title)
            if engine:
                engine = re.search('\d{3,4}', engine.group())
            if not engine:
                engine = re.search('(?<!\d)(\d{3})(?!\d)', title)
            if not engine:
                engine = re.search('(?<!\d)(1[0-8]\d{2})(?!\d)', title)
            if engine and int(engine.group()) > 0:
                params['engine'] = int(engine.group())

            make = re.search('|'.join(makes), title)
            if make:
                params['make'] = make.group()

            bikes.append(params)
        return bikes

    def getPics(self, url):
        thumbs = [];

        r = requests.get(url)
        soup = BeautifulSoup(r.text)
        try:
            pic = soup.select('#iwi')[0]['src']
        except:
            pic = ''

        try:
            thumbsRaw = soup.select('#thumbs')[0].findAll('a')
            for thumb in thumbsRaw:
                thumbs.append({
                    'url': thumb['href'],
                    'src': thumb.findAll('img')[0]['src']
                    })
        except:
            thumbs = False

        if thumbs:
            return {'thumbs': thumbs}
        else:
            return {'pic': pic}

