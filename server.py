from flask import Flask, request, make_response, g

import json
import os

from parser import Parser


# CONFIG
DEBUG = True
app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))

# ROUTES
@app.route('/')
def index():
    return make_response(open('templates/index.html').read())

@app.route('/bikes', methods=['GET'])
def getBikes():
    params = request.args.get('params') if request.args.get('params') else ''
    try:
        params = json.loads(params)
    except:
        params = {}
    parser = Parser()
    data = parser.parse(params)
    return json.dumps({'success': True, 'data': data})

@app.route('/pics', methods=['GET'])
def getPics():
    url = request.args.get('url')
    parser = Parser()
    data = parser.getPics(url)
    return json.dumps({'success': True, 'data': data})

if __name__ == '__main__':
    app.debug = True
    app.run(port=3030)

